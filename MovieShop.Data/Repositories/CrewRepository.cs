﻿using MovieShop.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShop.Data.Repositories
{

    public class CrewRepository : Repository<Crew>, ICrewRepository
    {
        public CrewRepository(MovieShopDbContext context) : base(context)
        {

        }

        public Crew GetCrewWithMovies(int crewId)
        {
            var crew = _context.Crews.Where(c => c.Id == crewId).Include(c => c.MovieCrews.Select(m => m.Movie))
                               .FirstOrDefault();
            return crew;
        }

    }

    public interface ICrewRepository : IRepository<Crew>
    {
        Crew GetCrewWithMovies(int crewId);

    }
}
