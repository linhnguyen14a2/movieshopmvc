﻿using MovieShop.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShop.Data.Repositories
{

    public class GenreRepository : Repository<Genre>, IRepository<Genre>
    {
        public GenreRepository(MovieShopDbContext context) : base(context)
        {
        }


    }
    public interface IGenreRepository : IRepository<Genre>
    {
        //IEnumerable<Genre> GetAllGenres();
    }


}
