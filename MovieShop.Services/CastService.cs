﻿using MovieShop.Data;
using MovieShop.Data.Repositories;
using MovieShop.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShop.Services
{
    public class CastService : ICastService
    {
        private readonly ICastRepository _castRepository;

        public CastService()
        {
            _castRepository = new CastRepository(new MovieShopDbContext());
        }

        public void CreateCast(Cast Cast)
        {
            _castRepository.Add(Cast);
        }

        public IList<Cast> GetAllCast()
        {
            return _castRepository.GetAll().ToList();
        }

        public Cast GetCastDetails(int castId)
        {
            var cast = _castRepository.GetCastWithMovies(castId);
            return cast;
        }
    }

    public interface ICastService
    {
        Cast GetCastDetails(int castId);

        void CreateCast(Cast Cast);

        IList<Cast> GetAllCast();
    }

}
