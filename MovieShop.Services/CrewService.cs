﻿using MovieShop.Data.Repositories;
using MovieShop.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShop.Services
{
    public class CrewService : ICrewService
    {
        private CrewRepository _crewRepository;

        public CrewService()
        {
            _crewRepository = new CrewRepository(new Data.MovieShopDbContext());
        }

        public void CreateCrew(Crew crew)
        {
            _crewRepository.Add(crew);
        }

        public List<Crew> GetAllCrews()
        {
            return _crewRepository.GetAll().ToList();
        }

        public Crew GetCrewDetails(int crewId)
        {
            var crew = _crewRepository.GetCrewWithMovies(crewId);
            return crew;
        }

    }

    public interface ICrewService
    {
        Crew GetCrewDetails(int crewId);

        void CreateCrew(Crew crew);

        List<Crew> GetAllCrews();
    }
}

