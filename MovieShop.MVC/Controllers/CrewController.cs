﻿using MovieShop.MVC.ViewModel;
using MovieShop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieShop.MVC.Controllers
{

    [RoutePrefix("crew")]
    public class CrewController : Controller
    {
        private CrewService _crewService;

        public CrewController()
        {
            _crewService = new CrewService();
        }

        // GET: Crew
        [HttpGet]
        [Route("details/{id}")]
        public ActionResult Details(int id)
        {
            var crew = _crewService.GetCrewDetails(id);
            var crewDetailsViewModel = new CrewDetailsViewModel();
            crewDetailsViewModel.Id = crew.Id;
            crewDetailsViewModel.Name = crew.Name;
            crewDetailsViewModel.Movies = new List<MovieDetailsViewModel>();

            foreach (var movie in crew.MovieCrews)
            {
                crewDetailsViewModel.Movies.Add(new MovieDetailsViewModel() { Id = movie.MovieId, Title = movie.Movie.Title });

            }
            return View(crewDetailsViewModel);

        }
        [HttpGet]
        [Route("Index")]//localhost/Crew/Index
        public ActionResult Index()
        {
            var crews = _crewService.GetAllCrews();
            return View(crews);
        }
      

    }

}