﻿using MovieShop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieShop.MVC.Controllers
{
    public class GenreController : Controller
    {
        private readonly GenreService _genreService;

        public GenreController()
        {
            _genreService = new GenreService();
        }

        // GET: Genre


        //public ActionResult Index()
        //{
        //    return View();

        //}



        public PartialViewResult Index()
        {
            return PartialView("GenreView", _genreService.GetAllGenres().OrderBy(g => g.Name).ToList());
        }

    }
}