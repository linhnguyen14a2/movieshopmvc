﻿using MovieShop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieShop.MVC.Controllers
{
    public class MoviesController : Controller
    {
        private MovieService _movieService;
        public MoviesController()
        {
            _movieService = new MovieService();
        }
        // GET: Movies
        public ActionResult Index()
        {

            var movies = _movieService.GetTopGrossingMovies();
            return View(movies);
        }
    }
}