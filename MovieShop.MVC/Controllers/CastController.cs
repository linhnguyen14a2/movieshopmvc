﻿using MovieShop.Entities;
using MovieShop.MVC.ViewModel;
using MovieShop.Services;
using MovieShop.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MovieShop.MVC.Controllers
{
    [RoutePrefix("cast")]
    public class CastController : Controller
    {
        private CastService _castService;

        public CastController()
        {
            _castService = new CastService();
        }

        // GET: Cast
        [HttpGet]
        [Route("details/{id}")]
        public ActionResult Details(int id)
        {
            var cast = _castService.GetCastDetails(id);
            var castDetailsViewModel = new CastDetailsViewModel();
            castDetailsViewModel.Id = cast.Id;
            castDetailsViewModel.Name = cast.Name;
            castDetailsViewModel.ProfilePath = cast.ProfilePath;
            castDetailsViewModel.TmdbUrl = cast.TmdbUrl;
            castDetailsViewModel.Gender = cast.Gender;
            castDetailsViewModel.Movies = new List<MovieViewModel>();

            foreach (var movie in cast.MovieCasts)
            {
                castDetailsViewModel.Movies.Add(new MovieViewModel() {Id=movie.MovieId,Title = movie.Movie.Title });

            }
            return View(castDetailsViewModel);

        }
        [HttpGet]
        [Route("Index")]//localhost/Cast/Index
        public ActionResult Index()
        {
            var casts = _castService.GetAllCast();
            return View(casts);
        }
        [HttpGet]
        public ActionResult Create()
        {
            //Cast db = new Cast();
            //List<Cast> listOfCast = 
            return View();
        }

        //Model Binding:pass by name or object in the params
        //
        [HttpPost]
        public ActionResult Create(Cast cast)
        {
            _castService.CreateCast(cast);
            ////save to database
            //using (var x = new DbContext)
            MovieShopDbContext db = new MovieShopDbContext();
            db.Casts.Add(new Cast { Name = cast.Name, Gender = cast.Gender, TmdbUrl = cast.TmdbUrl, ProfilePath = cast.ProfilePath });
            db.SaveChanges();


            return View("cast");
           

        }

    }
}