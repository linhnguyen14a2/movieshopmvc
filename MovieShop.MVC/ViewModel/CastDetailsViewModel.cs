﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieShop.MVC.ViewModel
{
    public class CastDetailsViewModel
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public String ProfilePath { get; set; }
        public String TmdbUrl { get; set; }
        public String Gender { get; set; }
        public List<MovieViewModel> Movies { get; set; }

    }
}