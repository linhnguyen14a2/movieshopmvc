﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieShop.MVC.ViewModel
{
    public class MovieDetailsViewModel
    {
        public int Id { get; set; }
        public String Title { get; set; }
        public String PosterUrl { get; set; }
        public int MyProperty { get; set; }
        public int Revenue { get; set; }
        public String ImdbUrl { get; set; }

        public List<CastDetailsViewModel> Casts { get; set; }

        public List<CrewDetailsViewModel> Crews { get; set; }

    }
}