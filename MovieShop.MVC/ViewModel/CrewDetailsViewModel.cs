﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieShop.MVC.ViewModel
{
    public class CrewDetailsViewModel
    {
        public int Id { get; set; }
        public String Name { get; set; }
        public List<MovieDetailsViewModel> Movies { get; set; }

    }
}