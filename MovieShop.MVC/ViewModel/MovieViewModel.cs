﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieShop.MVC.ViewModel
{
    public class MovieViewModel
    {
        public int Id { get; set; }
        public String Title { get; set; }
    }
}