﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShop.Entities
{
    [Table("Cast")]
    public class Cast
    {
        public int Id { get; set; }

        [StringLength(128)]
        public string Name { get; set; }


        private string gender;
        [MaxLength]
        public string Gender {

            get { return gender; }
            set { if (value == "2") gender = "Male";
                else gender = "Female";
                        }
        }
        [MaxLength]
        public string TmdbUrl { get; set; }

        [StringLength(2048)]
        public string ProfilePath { get; set; }

        public ICollection<MovieCast> MovieCasts { get; set; }
    }
}
