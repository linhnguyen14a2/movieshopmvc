﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieShop.Entities
{
    [Table("Trailer")]
    public class Trailer
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int MovieId { get; set; }
        public Movie Movie { get; set; }//navigation prop, get related informaiton
    }
}
